# init

TEMP_REPO_NAME="_temp"


# user setup

echo " "
echo "   //================================//"
echo "  // LIQUIDFUSION HTML5 BOILERPLATE //"
echo " //================================//"
echo " "
echo "SET UP PROJECT"
echo " "

echo "Project Name (no spaces): "
read name

echo " "
echo "Repository URI: "
read repo


# setting up repo

echo " "
echo "SETTING UP REPOSITORY"
echo " "

mv $TEMP_REPO_NAME $name
cd $name
rm -rf .git
git init
git remote add origin $repo
git add package.json
git commit -m "init"
git push -u origin master
