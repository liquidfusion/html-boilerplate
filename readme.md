# LiquidFusion HTML Boilerplate

This repository is used as boilerplate when starting a new project. Simply use the following command to start a new project:

```


git clone https://bitbucket.org/liquidfusion/html-boilerplate.git _temp && source _temp/install.sh


```

